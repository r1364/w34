import React from "react";

const imageBrowser = (props) => {
    return <h3>Finnish nature picture {props.year} <img src={props.imgURL}></img></h3>
};

export default imageBrowser;