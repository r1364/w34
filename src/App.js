import React, {Component} from 'react';
import ImageBrowser from './ImageBrowser'
import './App.css';

class App extends Component {
  render() {
  return (
    <div className="App">
     <ImageBrowser year="2021" imgURL="http://www.vuodenluontokuva.fi/vlk/userfiles/vlk2021/sarjavoittajat/e.1._liskomies_pekka%20tuuri.jpg"/>
    </div>
  );
}
}
export default App;
